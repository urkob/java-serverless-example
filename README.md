# java-serverless-example

## pre-install

### 1- [AWS CLI](https://aws.amazon.com/es/cli/)

Choose between A) or B)

- A) bash

```sh
pamac install aws-cli
```

- B) manjaro pamac GUI search for: aws-cli and install latest version (current 1.18.223-1)

### 2- [AWS SAM](https://github.com/aws/serverless-application-model#get-started) - https://aws.amazon.com/serverless/sam/

## Post pre-install

```sh
aws configure
```

And follow instructions to add your AWS credentials (access_key_id and access_secret)

## install

```sh
cd functions/SNSPublisher
mvn clean install
```

## local test AWS functions

```sh
sam build
sam local invoke "SNSPublisher" --profile adm
```

## deploy in AWS

```sh
sam build
sam deploy -g --profile YOUR_PROFILE
```

default profile is _default_
