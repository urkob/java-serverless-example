package SNSConsumer.src.main.java.snsConsumer;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;

import java.lang.System;
import software.amazon.awssdk.services.sns.model.PublishResponse;

/**
 * Handler for requests to Lambda function.
 */
public class App implements RequestHandler<SNSEvent, Object> {

    public PublishResponse handleRequest(SNSEvent request, Context context) {

        try {
            request.getRecords().forEach((k) -> System.out.println(k.getSNS().getMessage()));
            return null;
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            return null;
        }
    }
}
