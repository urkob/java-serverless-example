package SNSPublisher.src.main.java.snsPublisher;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.lang.System;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.http.crt.AwsCrtAsyncHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsAsyncClient;
import software.amazon.awssdk.services.sns.SnsAsyncClientBuilder;

import software.amazon.awssdk.services.sns.model.CreateTopicRequest;
import software.amazon.awssdk.services.sns.model.CreateTopicResponse;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;
import software.amazon.awssdk.services.sns.model.SnsException;

import com.acsystems.aws.sns.SNSClient;
import com.acsystems.aws.sns.SNSClientFactory;

/**
 * Handler for requests to Lambda function.
 */
public class App implements RequestHandler<Map<String, String>, PublishResponse> {
    private final Random rand = new Random();

    public PublishResponse handleRequest(Map<String, String> event, Context context) {

        try {
            SNSClient snsClient = SNSClientFactory.build();

            String accountId = System.getenv("ACCOUNT_ID");
            String region = System.getenv("REGION");
            String topicName = System.getenv("TOPIC_NAME");

            String arn = "arn:aws:sns:" + region + ":" + accountId + ":compare-pam";
            String message = "This is message from publisher";
            PublishRequest request = snsClient.buildPublishMessage(arn, message);

            java.util.concurrent.CompletableFuture<PublishResponse> futureResult = snsClient
                    .publishMessageAsync(request);

            PublishResponse result = futureResult.get();
            System.out
                    .println(result.messageId() + " Message sent. Status was " + result.sdkHttpResponse().statusCode());
            return result;
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            return null;
        }
    }
}
